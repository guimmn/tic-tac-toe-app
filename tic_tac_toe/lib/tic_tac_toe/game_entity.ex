defmodule TicTacToe.GameEntity do
  use TicTacToe.BaseSchema
  import Ecto.Changeset

  schema "games" do
    field :name, :string
    field :state, {:array, :string}

    timestamps()
  end

  @doc false
  def changeset(game_entity, attrs) do
    game_entity
    |> cast(attrs, [:name, :state])
    |> validate_required([:name, :state])
  end
end
