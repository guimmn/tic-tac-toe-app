defmodule TicTacToe.Games do
  # where I dump logic that I dont' know where else to put
  # it's just a wrapper to bring everything together (repo, game-servers, pubsub, channels, etc.)

  alias TicTacToe.{GameServer, GameSupervisor, GameEntity, Repo}

  # create game
  defp create_game(name) do
    # register game in BD
    insert_result =
      Repo.insert(%GameEntity{name: name, state: [nil, nil, nil, nil, nil, nil, nil, nil, nil]})

    # launch game server upon successful insertion
    # returns the server start result
    case insert_result do
      {:ok, %GameEntity{id: uuid}} ->
        case GameSupervisor.launch_game(uuid, name) do
          {:ok, _pid} -> {:ok, uuid}
          _ -> {:error, "couldn't start game server"}
        end

      {:error, _} ->
        {:error, "couldn't start game"}
    end
  end

  # create and join
  def create_and_join_game(name, player_name) do
    case create_game(name) do
      {:ok, uuid} -> join_game(uuid, player_name)
      {:error, msg} -> {:error, msg}
    end
  end

  # join game
  def join_game(uuid, player_name) do
    GameServer.join(uuid, player_name)
  end

  # list games
  def list_games do
    GameEntity
    |> Repo.all()
  end

  # make move
  def make_move(uuid, player_figure, position) do
    result = GameServer.make_move(uuid, player_figure, position)

    case result do
      {:error, msg} ->
        {:error, msg}

      {:ok, state} ->
        {:ok, state}

      _ ->
        %GameEntity{id: uuid} |> Repo.delete()
        result
    end
  end

  # leave game
  def quit_game(uuid, player_figure) do
    case GameServer.quit(uuid, player_figure) do
      {:empty, _state} -> Repo.delete(%GameEntity{id: uuid})
      {:ok, state} -> {:ok, state}
    end
  end
end
