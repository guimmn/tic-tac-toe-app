defmodule TicTacToe.Board do
  @icons [:x, :o]
  defstruct state: [
              nil,
              nil,
              nil,
              nil,
              nil,
              nil,
              nil,
              nil,
              nil
            ]

  def make_move(board, icon, pos) when icon in @icons and pos >= 0 and pos <= 8 do
    case Enum.at(board.state, pos) do
      nil ->
        new_state = List.replace_at(board.state, pos, icon)
        {:ok, %__MODULE__{board | state: new_state}}

      _ ->
        {:error, "position taken"}
    end
  end

  def full?(%__MODULE__{state: state}) do
    Enum.all?(state)
  end

  def winner(%__MODULE__{state: [i, i, i, _, _, _, _, _, _]}) when i in @icons, do: i
  def winner(%__MODULE__{state: [_, _, _, i, i, i, _, _, _]}) when i in @icons, do: i
  def winner(%__MODULE__{state: [_, _, _, _, _, _, i, i, i]}) when i in @icons, do: i
  def winner(%__MODULE__{state: [_, i, _, _, i, _, _, i, _]}) when i in @icons, do: i
  def winner(%__MODULE__{state: [i, _, _, i, _, _, i, _, _]}) when i in @icons, do: i
  def winner(%__MODULE__{state: [_, _, i, _, _, i, _, _, i]}) when i in @icons, do: i
  def winner(%__MODULE__{state: [_, _, i, _, i, _, i, _, _]}) when i in @icons, do: i
  def winner(%__MODULE__{state: [i, _, _, _, i, _, _, _, i]}) when i in @icons, do: i
  def winner(_), do: nil
end
