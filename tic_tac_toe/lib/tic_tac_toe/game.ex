defmodule TicTacToe.Game do
  defstruct board: %TicTacToe.Board{},
            id: nil,
            name: nil,
            first: :x,
            this_turn: :x,
            x: nil,
            o: nil,
            finished: false,
            winner: nil

  def new(name) do
    %__MODULE__{name: name}
  end

  def switch_turn(%__MODULE__{} = state) do
    case state do
      %__MODULE__{this_turn: :x} -> %__MODULE__{state | this_turn: :o}
      _ -> %__MODULE__{state | this_turn: :x}
    end
  end

  def finish(%__MODULE__{} = state, winner) do
    %__MODULE__{state | winner: winner, finished: true}
  end

  def finish(%__MODULE__{} = state) do
    %__MODULE__{state | finished: true}
  end

  def drop_player(%__MODULE__{} = state, player), do: %{state | player => nil}

  def reset(%__MODULE__{} = state),
    do: %__MODULE__{state | board: %TicTacToe.Board{}, finished: false}

  def empty?(%__MODULE__{} = state) do
    case state do
      %__MODULE__{o: nil, x: nil} -> true
      _ -> false
    end
  end
end
