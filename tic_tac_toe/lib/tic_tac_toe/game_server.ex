defmodule TicTacToe.GameServer do
  use GenServer
  alias TicTacToe.Game
  alias TicTacToe.Board

  # start_link -> launch_process
  def start_link(uuid, game_name) do
    # start server and register the game in the process registry
    GenServer.start_link(__MODULE__, {uuid, game_name}, name: via_tuple(uuid))
  end

  # from docs:
  # to get correct server (game) pid by name
  defp via_tuple(name) do
    {:via, Registry, {:game_server, name}}
  end

  # join a game
  def join(game_name, player) do
    via_tuple(game_name)
    |> GenServer.call({:join, player})
  end

  # make a move
  def make_move(game_name, player, position) do
    via_tuple(game_name)
    |> GenServer.call({:move, player, position})
  end

  # quit game
  def quit(game_name, :x) do
    via_tuple(game_name)
    |> GenServer.call({:quit, :x})
  end

  def quit(game_name, :o) do
    via_tuple(game_name)
    |> GenServer.call({:quit, :o})
  end

  def quit(_game_name, _player) do
    {:error, "invalid player"}
  end

  def get_state(game_name) do
    via_tuple(game_name)
    |> GenServer.call({:get_state})
  end

  # init -> register game server process in registry
  @impl true
  def init({uuid, game_name}) do
    {:ok, %Game{id: uuid, name: game_name}}
  end

  @impl true
  def handle_call({:join, player}, _from, %{x: nil} = state) do
    new_state = %{state | x: player}
    {:reply, {:ok, :x, new_state}, new_state}
  end

  @impl true
  def handle_call({:join, player}, _from, %{o: nil} = state) do
    new_state = %{state | o: player}
    {:reply, {:ok, :o, new_state}, new_state}
  end

  @impl true
  def handle_call({:join, _player}, _from, state) do
    {:reply, {:error, "game is full"}, state}
  end

  @impl true
  def handle_call({:move, player, position}, _from, %Game{this_turn: player} = state) do
    case Board.make_move(state.board, player, position) do
      # position already taken
      {:error, msg} ->
        {:reply, {:error, msg, state}, state}

      # position not taken
      {:ok, board} ->
        state = %Game{state | board: board}
        # verify conditions for ending game
        cond do
          # verify if the board is now full
          Board.full?(state.board) ->
            new_state = Game.finish(state)
            {:reply, {:tie, new_state}, new_state}

          # verify if there is a winner
          winner = Board.winner(state.board) ->
            new_state = Game.finish(state, winner)
            # for now I'm sending a stop
            # could just send a reply, but the game is over so it makes sense I guess
            {:stop, :normal, {:winner, winner, new_state}, new_state}

          # game carries on
          true ->
            new_state = Game.switch_turn(state)
            {:reply, {:ok, new_state}, new_state}
        end
    end
  end

  @impl true
  def handle_call({:move, _player, _position}, _from, %Game{} = state),
    do: {:reply, {:error, "not your turn", state}, state}

  @impl true
  def handle_call({:quit, player}, _from, %Game{} = state) do
    new_state =
      state
      |> Game.drop_player(player)
      |> Game.reset()

    cond do
      Game.empty?(new_state) ->
        # just stop the process
        # it will be deregistered form the game_server_registry automatically
        # I hope it's also removed from the active_games registry
        # after reading the docs I believe it is, but I'm not sure
        {:stop, :normal, {:empty, new_state}, new_state}

      # otherwise, just reset the game, the player can wait for another one
      true ->
        {:reply, {:ok, new_state}, new_state}
    end
  end

  @impl true
  def handle_call({:get_state}, _from, %Game{} = state), do: {:reply, {:ok, state}, state}
end
