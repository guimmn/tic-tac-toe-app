defmodule TicTacToe.GameSupervisor do
  use DynamicSupervisor
  alias TicTacToe.GameServer

  # dynamic supervisor to laucnh and oversee game genservers

  def start_link(init_arg \\ nil) do
    DynamicSupervisor.start_link(__MODULE__, init_arg, name: __MODULE__)
  end

  @impl true
  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def launch_game(uuid, game_name) do
    child_spec = %{
      id: GameServer,
      start: {GameServer, :start_link, [uuid, game_name]},
      restart: :temporary
    }

    DynamicSupervisor.start_child(
      {:via, PartitionSupervisor, {TicTacToe.DynamicSupervisors, self()}},
      child_spec
    )
  end

  def is_game_online?(uuid) do
    case Registry.lookup(:game_server, uuid) do
      [] -> false
      _ -> true
    end
  end
end
