defmodule TicTacToeWeb.PageController do
  use TicTacToeWeb, :controller
  alias TicTacToe.Games

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.

    # test
    data =
      Games.list_games()
      |> Enum.map(fn %TicTacToe.GameEntity{id: uuid, name: name} -> %{id: uuid, name: name} end)

    render(conn, :home, games: data)
  end
end
