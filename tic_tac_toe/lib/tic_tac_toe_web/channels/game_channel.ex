defmodule TicTacToeWeb.GameChannel do
  use Phoenix.Channel
  alias TicTacToe.Games

  # join (to listen to changes down the line on this topic)
  def join("game:" <> id, _message, socket) do
    {:ok, socket}
  end

  def handle_in("ping", message, socket) do
    broadcast!(socket, "ping", message)
    {:noreply, socket}
  end

  # make move
  def handle_in("move", %{"id" => id, "figure" => figure, "position" => position}, socket) do
    # call Games.make_move here and treat accordingly
    case Games.make_move(id, figure, position) do
      {:ok, state} ->
        broadcast!(socket, "move", %{state: state})

      {:tie, state} ->
        broadcast!(socket, "move", %{state: state})

      {:winner, _winner, state} ->
        broadcast!(socket, "move", %{state: state})

      # hit invalid position, no reply, keep the state it was at
      _ ->
        {:noreply, socket}
    end
  end

  # quit
  def handle_in("quit", %{"id" => id, "figure" => figure}, socket) do
    case Games.quit_game(id, figure) do
      {:ok, state} ->
        broadcast!(socket, "quit", %{state: state})

      _ ->
        {:noreply, socket}
    end
  end
end
